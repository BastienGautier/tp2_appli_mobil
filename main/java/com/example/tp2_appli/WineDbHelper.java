package com.example.tp2_appli;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AlertDialog;
import android.util.Log;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 2;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+TABLE_NAME+ " (" +
                _ID + " INTEGER PRIMARY KEY , " +
                COLUMN_NAME + " TEXT NOT NULL, " +
                COLUMN_WINE_REGION + " TEXT NOT NULL, " +
                COLUMN_LOC + " TEXT NOT NULL, " +
                COLUMN_CLIMATE + " TEXT NOT NULL, " +
                COLUMN_PLANTED_AREA + " INTEGER NOT NULL," +
                "UNIQUE("+COLUMN_NAME+","+COLUMN_WINE_REGION+") ON CONFLICT ROLLBACK);"
        );

        // db.execSQL() with the CREATE TABLE ... command
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Typically do ALTERTABLEstatements, but...we're justin development,
        if (oldVersion < newVersion) {
            db.execSQL("DROP TABLE IF EXISTS '" + TABLE_NAME + "'");        // dropsthe old database
            onCreate(db);       // run onCreate to get new database
        }
    }


   /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */

    public boolean addWine(Wine wine) throws Exception {

        SQLiteDatabase db = this.getWritableDatabase();
        // Inserting Row
        long rowID = 0;
        // Insertvalue
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_NAME,wine.getTitle());
        cv.put(COLUMN_WINE_REGION,wine.getRegion());
        cv.put(COLUMN_LOC,wine.getLocalization());
        cv.put(COLUMN_CLIMATE,wine.getClimate());
        cv.put(COLUMN_PLANTED_AREA,wine.getPlantedArea());

        db.insert(TABLE_NAME, null, cv);
        rowID=1;

        // Closethe database
        db.close();



        return rowID!=0;
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) throws Exception {
        SQLiteDatabase db = this.getWritableDatabase();
	    int res =0;


        Log.d(MainActivity.TAG, "UPDATE WineDB");
        // updating row
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_NAME,wine.getTitle());
        cv.put(COLUMN_WINE_REGION,wine.getRegion());
        cv.put(COLUMN_LOC,wine.getLocalization());
        cv.put(COLUMN_CLIMATE,wine.getClimate());
        cv.put(COLUMN_PLANTED_AREA,wine.getPlantedArea());

        db.update(TABLE_NAME, cv, _ID+"=?", new String[]{String.valueOf(wine.getId())});
        res=1;

        db.close();
        
        return res;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {
        SQLiteDatabase db = this.getReadableDatabase();

	    Cursor cursor=db.query(TABLE_NAME,null,null,null,null,null,null);
        
        if (cursor != null) {
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }

     public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
         Log.d(TAG, "Delete Wine");
         if(cursor!=null) {
             Wine wine = WineDbHelper.cursorToWine(cursor);
             Log.d(TAG, "item on click=" + wine.toString());
             db.delete(TABLE_NAME,_ID+"=?", new String[]{String.valueOf(wine.getId())});
         }
         else
             Log.d(TAG, "CURSOR NULL");
        // call db.delete();
        db.close();
    }



     public void populate() {
        Log.d(TAG, "call populate()");
        try {
            addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
            addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
            addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
            addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
            addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
            addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
            addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
            addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
            addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
            addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
            addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));
        }
        catch (Exception e){

        }


        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }


    public static Wine cursorToWine(Cursor cursor) {
        if(cursor==null)
            return null;
        Wine wine = new Wine(
                cursor.getLong(cursor.getColumnIndex(_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_NAME)),
                cursor.getString(cursor.getColumnIndex(COLUMN_WINE_REGION)),
                cursor.getString(cursor.getColumnIndex(COLUMN_LOC)),
                cursor.getString(cursor.getColumnIndex(COLUMN_CLIMATE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_PLANTED_AREA))
        );

	// build a Wine object from cursor
        return wine;
    }


    public Cursor fetchWine(String name, String region) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selection = COLUMN_NAME+" =? AND "+COLUMN_WINE_REGION+"=?";
        String []arg = {name,region};

        Cursor cursor=db.query(TABLE_NAME,null,selection,arg,null,null,null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }
}
