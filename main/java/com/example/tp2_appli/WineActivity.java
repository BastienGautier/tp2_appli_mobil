package com.example.tp2_appli;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;


public class WineActivity extends AppCompatActivity {
    EditText wineName;
    EditText editWineRegion;
    EditText editLoc;
    EditText editClimate;
    EditText editPlantedArea;

    boolean isNew;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        wineName =(EditText) findViewById(R.id.wineName);
        editWineRegion=(EditText) findViewById(R.id.editWineRegion);
        editLoc=(EditText) findViewById(R.id.editLoc);
        editClimate=(EditText) findViewById(R.id.editClimate);
        editPlantedArea=(EditText) findViewById(R.id.editPlantedArea);

        Intent secondIntent = getIntent();

        final Wine wine= secondIntent.getExtras().getParcelable("WINE_CLICK");

        isNew=false;
        if(wine.oneIsEmpty())
            isNew=true;

        wineName.setText(wine.getTitle());
        editWineRegion.setText(wine.getRegion());
        editLoc.setText(wine.getLocalization());
        editClimate.setText(wine.getClimate());
        editPlantedArea.setText(wine.getPlantedArea());

        Button sendModif = (Button) findViewById(R.id.button);
        sendModif.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                wine.setTitle(wineName.getText().toString());
                wine.setRegion(editWineRegion.getText().toString());
                wine.setLocalization(editLoc.getText().toString());
                wine.setClimate(editClimate.getText().toString());
                wine.setPlantedArea(editPlantedArea.getText().toString());

                AlertDialog.Builder alertDialogBuilder=null;

                String error = "Error ";

                if(wine.oneIsEmpty()){
                    alertDialogBuilder = new AlertDialog.Builder(WineActivity.this);
                    alertDialogBuilder.setTitle("Something is empty");
                    alertDialogBuilder.setMessage("Everything need to be complete");
                }
                else {
                    try{
                        if(isNew)
                        {
                            error += "addWine";
                            MainActivity.WINEDB.addWine(wine);
                        }
                        else
                        {
                            error += "UpdateWine";
                            MainActivity.WINEDB.updateWine(wine);
                        }
                    }
                    catch (Exception e)
                    {
                        Toast.makeText(v.getContext(),error, Toast.LENGTH_LONG).show();
                        alertDialogBuilder = new AlertDialog.Builder(WineActivity.this);
                        alertDialogBuilder.setTitle(error);
                        alertDialogBuilder.setMessage( e.getMessage());
                        alertDialogBuilder.show();
                    }
                    MainActivity.getInstance().updateCursor();
                }
                if(alertDialogBuilder!=null)
                    alertDialogBuilder.show();
                else
                {
                    alertDialogBuilder = new AlertDialog.Builder(WineActivity.this);
                    alertDialogBuilder.setTitle(error);
                    alertDialogBuilder.setMessage( "This wine already exist");
                    alertDialogBuilder.show();
                }

            }
        });
    }
}
