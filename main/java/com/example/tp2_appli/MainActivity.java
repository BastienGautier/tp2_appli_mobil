package com.example.tp2_appli;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static MainActivity MAINACTIVITY;
    protected static WineDbHelper WINEDB;

    public static final String TAG = WineDbHelper.class.getSimpleName();

    ListView listWine;
    Cursor allWine;

    Intent myIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        MAINACTIVITY=this;

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myIntent.putExtra("WINE_CLICK", new Wine());
                startActivity(myIntent);
            }
        });


        WINEDB = new WineDbHelper(getApplicationContext());
        WINEDB.populate();

        myIntent= new Intent(this,WineActivity.class);

        listWine = (ListView) findViewById(R.id.ListWine);

        updateCursor();

        listWine.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                myIntent.putExtra("WINE_CLICK", WineDbHelper.cursorToWine((Cursor)parent.getItemAtPosition(position)));
                startActivity(myIntent);

            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        switch (v.getId()){

            case R.id.ListWine:

                AdapterView.AdapterContextMenuInfo info =
                        (AdapterView.AdapterContextMenuInfo) menuInfo;

                allWine.moveToPosition(info.position);
            //    String wine = ((TextView) info.targetView).toString();
                menu.setHeaderTitle(allWine.getString(1));
                String [] actions = {"Delete"};
                for (int i=0;i<actions.length;i++){
                    menu.add(Menu.NONE, i, i, actions[i]);
                }
                break;

        }

    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int menuItemIndex = item.getItemId();
        String menuItemName = item.getTitle().toString();
        try {
            WINEDB.deleteWine(allWine);

            Toast.makeText(this, allWine.getString(1)+" is delete", Toast.LENGTH_LONG).show();
            MainActivity.getInstance().updateCursor();
            return true;
        }
        catch (Exception e)
        {
            Toast.makeText(this, "Error delete "+allWine.getString(1), Toast.LENGTH_LONG).show();
            return false;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void updateCursor()
    {
        allWine=WINEDB.fetchAllWines();
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(getApplicationContext(),
                android.R.layout.simple_list_item_2,allWine,
                new String[] {WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION},
                new int[]{ android.R.id.text1,android.R.id.text2}, 0);
        listWine.setAdapter(adapter);

        registerForContextMenu(listWine);
    }



    public static MainActivity getInstance(){
        return MAINACTIVITY;
    }


}

